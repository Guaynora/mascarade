import random

class Nodo(object):
    def __init__(self, nombre = None, moneda = None, carta = None, anterior = None, siguiente = None):
        self.nombre = nombre
        self.moneda = moneda
        self.carta = carta
        self.anterior = anterior
        self.siguiente = siguiente
    


class LinkedList:
    def __init__(self):
        self.inicio = None
        self.fin = None
        self.contador = 0

    def agregarInicio(self,nombre,moneda,carta):
        if(self.validar()):
            nuevo = Nodo(nombre,moneda,carta,None,None)
            self.inicio = nuevo
            self.fin = nuevo
        else:
            nuevo = Nodo(nombre,moneda,carta,None,self.inicio)
            self.inicio.anterior = nuevo
            self.inicio = nuevo
        
    def agregar(self):
        cartasxtras = ["ladron","Tahur"]
        tiposcartasx = random.sample(cartasxtras,1)
        nombre = "Carta1"
        moneda = None
        carta = tiposcartasx
        self.agregarInicio(nombre,moneda,carta)
        cartasxtras2 = ["ladron","Tahur"]
        tiposcartasx2 = random.sample(cartasxtras2,1)
        nombre = "Carta2"
        moneda = None
        carta = tiposcartasx2
        self.agregarInicio(nombre,moneda,carta)
        while self.contador != 4:
            cartas = ["Juez","Obispo","Rey","Reina","ladron","Tahur"]
            tiposcartas = random.sample(cartas,1)
            nombre = input("Digite su nombre: ")
            moneda = 6
            carta = tiposcartas
            temp = self.inicio
            while temp != None:
                if temp.nombre == nombre:
                    print("Error, este nombre ya existe", nombre)
                    temp = temp.anterior
                    self.eliminar(nombre)
                    break
                temp = temp.siguiente
            self.agregarInicio(nombre,moneda,carta)
            self.contador += 1

    def iterar(self):
        actual = self.inicio

        while actual:
            nombre = actual.nombre
            actual = actual.siguiente
            yield nombre    


    def eliminar(self, nombre):
        actual = self.inicio
        eliminado = False

        if actual is None:
            eliminado = False
        elif actual.nombre == nombre:
            self.inicio = actual.nombre
            self.inicio.anterior = None
            eliminado = True
        elif self.fin.nombre == nombre:
            self.fin = self.fin.anterior
            self.fin.siguiente = None
            eliminado = True
        else:
            while actual:
                if actual.nombre == nombre:
                    # Antes: 2 = 3 = 5 = 7 = 11
                    # Actual: 3
                    # Después: 2 = 5 = 7 = 11
                    actual.anterior.siguiente = actual.siguiente
                    actual.siguiente.anterior = actual.anterior
                    eliminado = True
                actual = actual.siguiente
        
        if eliminado:
            self.contador -= 1

    def vercarta(self):
        name = input("digite el nombre del jugador: ")
        temp = self.inicio
        while temp != None:
            if temp.nombre  == name:
                print("\nCarta: ", temp.carta)
            temp = temp.siguiente



    def imprimir(self):
        temp = self.inicio
        while temp != None:
            print (temp.nombre , str(temp.moneda) , temp.carta)
            temp = temp.siguiente

    def validar(self):
        if(self.inicio==None):
            return True
        return False

